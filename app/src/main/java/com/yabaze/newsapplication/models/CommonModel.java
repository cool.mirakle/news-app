package com.yabaze.newsapplication.models;

import android.os.Handler;

import androidx.lifecycle.MutableLiveData;

public class CommonModel {

    public MutableLiveData<String> actionName = new MutableLiveData<>();

    public MutableLiveData<String> message = new MutableLiveData<>();

    public MutableLiveData<Boolean> showMessage = new MutableLiveData<>();

    public CommonModel() {
        showMessage.postValue(false);
    }

    public void showToastMessage(String message) {
        this.message.postValue(message);
        this.showMessage.postValue(true);
        new Handler().postDelayed(() -> showMessage.postValue(false), 2000);
    }

}
