package com.yabaze.newsapplication.views.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.yabaze.newsapplication.R;
import com.yabaze.newsapplication.databinding.FilterDialogViewBinding;
import com.yabaze.newsapplication.viewModels.HomePageViewModel;

import java.util.Objects;

public class FilterDialogFragment extends DialogFragment {

    HomePageViewModel homePageViewModel;

    public FilterDialogFragment(HomePageViewModel homePageViewModel) {
        this.homePageViewModel = homePageViewModel;
    }

    FilterDialogViewBinding filterDialogViewBinding;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        filterDialogViewBinding = DataBindingUtil
                .inflate(LayoutInflater.from(getActivity()), R.layout.filter_dialog_view, null, false);

        filterDialogViewBinding.setViewModel(homePageViewModel);

        filterDialogViewBinding.setLifecycleOwner(this);

        Dialog dialog = new Dialog(Objects.requireNonNull(getContext()));
        dialog.setContentView(filterDialogViewBinding.getRoot());
        dialog.show();

        filterDialogViewBinding.applyFilterButton.setOnClickListener(v -> {

            if (homePageViewModel.validateFields()) {

                homePageViewModel.headLineSelected.setValue(true);

                dialog.dismiss();
            } else {
                Toast.makeText(getActivity(), "" + homePageViewModel.commonModel.message.getValue(), Toast.LENGTH_SHORT).show();
            }

        });

        filterDialogViewBinding.cancelButton.setOnClickListener(v -> {
            dialog.dismiss();
        });

        return dialog;

    }

}
