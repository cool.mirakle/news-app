package com.yabaze.newsapplication.views.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.yabaze.newsapplication.R;
import com.yabaze.newsapplication.databinding.ActivityNewsHomePageBinding;
import com.yabaze.newsapplication.event.NetworkEventBus;
import com.yabaze.newsapplication.repositories.remote.dto.Article;
import com.yabaze.newsapplication.services.NetworkReceiver;
import com.yabaze.newsapplication.viewModels.HomePageViewModel;
import com.yabaze.newsapplication.views.adapters.HomeViewPagerAdapter;
import com.yabaze.newsapplication.views.fragments.FilterDialogFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NewsHomePage extends AppCompatActivity {

    ActivityNewsHomePageBinding activityNewsHomePageBinding;

    HomePageViewModel homePageViewModel = new HomePageViewModel(getApplication());

    HomeViewPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityNewsHomePageBinding = DataBindingUtil.setContentView(this, R.layout.activity_news_home_page);
        activityNewsHomePageBinding.setLifecycleOwner(this);
        activityNewsHomePageBinding.setViewModel(homePageViewModel);

        homePageViewModel.callApi();

        homePageViewModel.newsResponseMutableLiveData.observe(this, apiResponsePojo -> {
            if (apiResponsePojo != null) {
                if (apiResponsePojo.getStatus().equals("ok")) {
                    if (apiResponsePojo.getArticles().size() > 0) {
                        HashMap<String, List<Article>> articlesBasedSourceName = new HashMap<>();

                        for (Article article :
                                apiResponsePojo.getArticles()) {
                            if (articlesBasedSourceName.containsKey(article.getSource().getName())) {
                                List<Article> list = articlesBasedSourceName.get(article.getSource().getName());
                                if (list != null) {
                                    list.add(article);
                                }
                                articlesBasedSourceName.put(article.getSource().getName(), list);
                            } else {
                                ArrayList<Article> list = new ArrayList<>();
                                list.add(article);
                                articlesBasedSourceName.put(article.getSource().getName(), list);
                            }

                        }

                        setViewPager(articlesBasedSourceName);

                        homePageViewModel.articlesBasedSourceName.postValue(articlesBasedSourceName);

                    }
                } else {
                    homePageViewModel.commonModel.showToastMessage("Sorry ...! Try AGain..!");
                }
            }
        });
        homePageViewModel.commonModel.actionName.observe(this, actionName -> {
            if (actionName != null && !actionName.isEmpty()) {
                Intent webViewIntent = new Intent(NewsHomePage.this, WebViewActivity.class);
                webViewIntent.putExtra("URL", homePageViewModel.webUrl.getValue());
                startActivity(webViewIntent);
            }
        });
        activityNewsHomePageBinding.materialToolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.filter) {
                new FilterDialogFragment(homePageViewModel).show(getSupportFragmentManager(), "FILTER_FRAGMENT");
            }
            return false;
        });
    }

    private void setViewPager(HashMap<String, List<Article>> articlesBasedSourceName) {
        pagerAdapter = new HomeViewPagerAdapter(this, articlesBasedSourceName, homePageViewModel);

        activityNewsHomePageBinding.homeNewsViewPager.setAdapter(
                pagerAdapter
        );

        ArrayList<String> tabTitles = new ArrayList<>(articlesBasedSourceName.keySet());

        new TabLayoutMediator(activityNewsHomePageBinding.tabLayout, activityNewsHomePageBinding.homeNewsViewPager, true, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {

                tab.setText(tabTitles.get(position));
                activityNewsHomePageBinding.homeNewsViewPager.setCurrentItem(tab.getPosition(), true);
            }
        }).attach();
    }


    NetworkReceiver networkStateReceiver = new NetworkReceiver();

    @Override
    protected void onStart() {
        super.onStart();
        this.registerReceiver(
                networkStateReceiver,
                new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        );

        EventBus.getDefault().register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        this.unregisterReceiver(networkStateReceiver);
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onNetworkEvent(NetworkEventBus networkEvent) {

        if (!networkEvent.getNetworkAvailable()) {
            homePageViewModel.commonModel.message.postValue("No Network Available..!");
            homePageViewModel.commonModel.showMessage.postValue(true);
        } else {
            homePageViewModel.commonModel.showMessage.postValue(false);
        }

    }


}