package com.yabaze.newsapplication.views.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.yabaze.newsapplication.repositories.remote.dto.Article;
import com.yabaze.newsapplication.viewModels.HomePageViewModel;
import com.yabaze.newsapplication.views.fragments.NewsPageFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeViewPagerAdapter extends FragmentStateAdapter {

    HashMap<String, List<Article>> articlesBasedSourceName;
    ArrayList<String> tabTitlesList = new ArrayList<>();

    HomePageViewModel homePageViewModel;

    public HomeViewPagerAdapter(FragmentActivity fa, HashMap<String, List<Article>> articlesBasedSourceName, HomePageViewModel homePageViewModel) {
        super(fa);
        this.articlesBasedSourceName = articlesBasedSourceName;
        tabTitlesList = new ArrayList<>(articlesBasedSourceName.keySet());
        this.homePageViewModel = homePageViewModel;
    }

    @Override
    public Fragment createFragment(int position) {

        if (tabTitlesList != null) {
            return new NewsPageFragment(articlesBasedSourceName.get(tabTitlesList.get(position)),homePageViewModel);
        } else {
            return new NewsPageFragment(new ArrayList<>(),homePageViewModel);
        }

    }

    @Override
    public int getItemCount() {
        return articlesBasedSourceName.keySet().size();
    }

}
