package com.yabaze.newsapplication.views.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.yabaze.newsapplication.R;
import com.yabaze.newsapplication.common.MyWebViewClient;
import com.yabaze.newsapplication.databinding.ActivityWebViewBinding;
import com.yabaze.newsapplication.event.NetworkEventBus;
import com.yabaze.newsapplication.services.NetworkReceiver;
import com.yabaze.newsapplication.viewModels.HomePageViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class WebViewActivity extends AppCompatActivity implements MyWebViewClient.WebViewError {

    String url;
    ActivityWebViewBinding activityWebViewBinding;
    HomePageViewModel viewModel;
    NetworkReceiver networkStateReceiver = new NetworkReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityWebViewBinding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);
        viewModel = new HomePageViewModel(getApplication());
        activityWebViewBinding.setViewModel(viewModel);

        if (getIntent() != null) {
            Intent intent = getIntent();
            url = intent.getStringExtra("URL");
            if (url == null) {
                Toast.makeText(this, "Sorry Try Again..!", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
        }

        activityWebViewBinding.webView.loadUrl(url);
        activityWebViewBinding.webView.setWebViewClient(new MyWebViewClient(this, this));

    }


    @Override
    public void onWebLoadFailed() {
        viewModel.commonModel.showToastMessage("Failed To Load Link..!");
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.registerReceiver(
                networkStateReceiver,
                new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        );

        EventBus.getDefault().register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        this.unregisterReceiver(networkStateReceiver);
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onNetworkEvent(NetworkEventBus networkEvent) {

        if (!networkEvent.getNetworkAvailable()) {
            viewModel.commonModel.message.postValue("No Network Available..!");
            viewModel.commonModel.showMessage.postValue(true);
        } else {
            viewModel.commonModel.showMessage.postValue(false);
        }

    }

}


