package com.yabaze.newsapplication.views.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yabaze.newsapplication.R;
import com.yabaze.newsapplication.databinding.FragmentNewsPageBinding;
import com.yabaze.newsapplication.repositories.remote.dto.Article;
import com.yabaze.newsapplication.viewModels.HomePageViewModel;
import com.yabaze.newsapplication.views.activities.NewsHomePage;
import com.yabaze.newsapplication.views.activities.WebViewActivity;
import com.yabaze.newsapplication.views.adapters.NewsRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NewsPageFragment extends Fragment {

    List<Article> newsArticleList;

    HomePageViewModel homePageViewModel;

    public NewsPageFragment() {
        this.homePageViewModel = new HomePageViewModel(Objects.requireNonNull(getActivity()).getApplication());
        this.newsArticleList = new ArrayList<>();
    }

    public NewsPageFragment(List<Article> articleArrayList, HomePageViewModel homePageViewModel) {
        this.newsArticleList = articleArrayList;
        this.homePageViewModel = homePageViewModel;
    }

    FragmentNewsPageBinding fragmentNewsPageBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentNewsPageBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_news_page, container, false);
        fragmentNewsPageBinding.setViewModel(homePageViewModel);
        fragmentNewsPageBinding.setLifecycleOwner(this);

        fragmentNewsPageBinding.newsRecyclerView.setLayoutManager(
                new LinearLayoutManager(
                        fragmentNewsPageBinding.newsRecyclerView.getContext(),
                        RecyclerView.VERTICAL, false));
        fragmentNewsPageBinding.newsRecyclerView.setAdapter(new NewsRecyclerViewAdapter(newsArticleList,homePageViewModel));

        return fragmentNewsPageBinding.getRoot();
    }


}