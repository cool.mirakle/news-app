package com.yabaze.newsapplication.repositories.remote;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.yabaze.newsapplication.BuildConfig;
import com.yabaze.newsapplication.repositories.remote.dto.ApiResponsePojo;
import com.yabaze.newsapplication.viewModels.HomePageViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoteRepository {

    private NewsApi newsApi;

    public RemoteRepository() {
        this.newsApi = RetrofitClient.getClient().create(NewsApi.class);
    }

    public MutableLiveData<ApiResponsePojo> getTopHeadLines() {

        MutableLiveData<ApiResponsePojo> newsData = new MutableLiveData<>();

        Call<ApiResponsePojo> headlineResponseCall = newsApi.headlineBasedOnCountry("us", BuildConfig.API_KEY);

        headlineResponseCall.enqueue(new Callback<ApiResponsePojo>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponsePojo> call, @NonNull Response<ApiResponsePojo> response) {
                if (response.isSuccessful()) {
                    newsData.setValue(response.body());
                } else {
                    newsData.setValue(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponsePojo> call, @NonNull Throwable t) {

                call.cancel();
                newsData.setValue(null);

            }
        });

        return newsData;

    }

    public void getTopHeadLines(String url, HomePageViewModel.ApiCall apiCall) {

        MutableLiveData<ApiResponsePojo> newsData = new MutableLiveData<>();

        Call<ApiResponsePojo> headlineResponseCall = newsApi.headlineBasedOnFilter(url);

        headlineResponseCall.enqueue(new Callback<ApiResponsePojo>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponsePojo> call, @NonNull Response<ApiResponsePojo> response) {
                if (response.isSuccessful()) {
                    apiCall.onSuccess(response.body());
                } else {
                    apiCall.onFailure();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponsePojo> call, @NonNull Throwable t) {

                call.cancel();
                apiCall.onFailure();

            }
        });

    }

}
