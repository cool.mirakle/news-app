package com.yabaze.newsapplication.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.yabaze.newsapplication.common.CommonUtilities;
import com.yabaze.newsapplication.event.NetworkEventBus;

import org.greenrobot.eventbus.EventBus;

public class NetworkReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getExtras() == null) {
            return;
        } else {
            EventBus.getDefault().post(new NetworkEventBus(CommonUtilities.checkInternetConnection(context)));
        }
    }
}
