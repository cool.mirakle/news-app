package com.yabaze.newsapplication.viewModels;

import android.app.Application;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.yabaze.newsapplication.BuildConfig;
import com.yabaze.newsapplication.GlideApp;
import com.yabaze.newsapplication.R;
import com.yabaze.newsapplication.models.CommonModel;
import com.yabaze.newsapplication.repositories.remote.RemoteRepository;
import com.yabaze.newsapplication.repositories.remote.dto.ApiResponsePojo;
import com.yabaze.newsapplication.repositories.remote.dto.Article;

import java.util.HashMap;
import java.util.List;

public class HomePageViewModel extends AndroidViewModel {

    RemoteRepository remoteRepository = new RemoteRepository();

    public MutableLiveData<ApiResponsePojo> newsResponseMutableLiveData;
    public MutableLiveData<HashMap<String, List<Article>>> articlesBasedSourceName = new MutableLiveData<>();

    public CommonModel commonModel = new CommonModel();

    public MutableLiveData<String> webUrl = new MutableLiveData<>();
    public MutableLiveData<String> source = new MutableLiveData<>();
    public MutableLiveData<String> keyword = new MutableLiveData<>();
    public MutableLiveData<String> countryCode = new MutableLiveData<>();
    public MutableLiveData<String> category = new MutableLiveData<>();
    public MutableLiveData<Boolean> hideSource = new MutableLiveData<>();
    public MutableLiveData<Boolean> headLineSelected = new MutableLiveData<>();

    public HomePageViewModel(@NonNull Application application) {
        super(application);
        source.setValue("");
        keyword.setValue("");
        countryCode.setValue("Select Country");
        category.setValue("Select Category");
        hideSource.setValue(false);
        headLineSelected.setValue(true);
    }

    @BindingAdapter(value = "setImage", requireAll = false)
    public static void setImage(ImageView view, String url) {

        if (url != null) {
            GlideApp.with(view.getContext())
                    .load(url)
                    .apply(new RequestOptions().circleCrop().transform(new RoundedCorners(5)))
                    .into(view);
        }
    }


    public void onSelectItem(AdapterView<?> parent, View view, int pos, Long id) {
        String answer = parent.getSelectedItem().toString();

        switch (parent.getId()) {
            case R.id.countryCode:
                countryCode.setValue(answer);
                break;
            case R.id.categoryCode:
                category.setValue(answer);
                break;
            default:

                break;
        }

        if (pos > 0) {
            hideSource.postValue(true);
            source.setValue("");
        } else {
            if (countryCode.getValue().equalsIgnoreCase("Select Country") && category.getValue().equalsIgnoreCase("Select Category")) {
                hideSource.postValue(false);
            }
        }

    }

    public void openArticleWebView(@NonNull Article article) {
        if (article.getUrl() != null) {
            commonModel.actionName.postValue("SHOW_WEB_VIEW");
            webUrl.setValue(article.getUrl());
        } else {
            commonModel.showToastMessage("Link Not Available..!");
        }
    }

    public void onTypeChanged(RadioGroup radioGroup, int id) {

        if (radioGroup.getCheckedRadioButtonId() == R.id.headLineRadioButton) {
            headLineSelected.setValue(true);
        } else {
            headLineSelected.setValue(false);
        }
    }

    public boolean validateFields() {

        if (
                (countryCode.getValue() == null || countryCode.getValue().equalsIgnoreCase("Select Country"))
                        && (category.getValue() == null || category.getValue().equalsIgnoreCase("Select Category"))
                        && (source.getValue() == null || source.getValue().isEmpty())
                        && (keyword.getValue() == null || keyword.getValue().isEmpty())
        ) {

            commonModel.message.setValue("Select Or Enter Any One Field..!");
            return false;
        }


        String url = "/v2/top-headlines?";

        if (!headLineSelected.getValue()) {
            url = "/v2/everything?";
        }


        if (headLineSelected.getValue()) {

            if (
                    (countryCode.getValue() != null && !countryCode.getValue().equalsIgnoreCase("Select Country"))

            ) {
                url += ("country=" + countryCode.getValue());

            }

            if ((category.getValue() != null && !category.getValue().equalsIgnoreCase("Select Category"))) {

                if (
                        (countryCode.getValue() != null && !countryCode.getValue().equalsIgnoreCase("Select Country"))

                ) {
                    url += "&";
                }

                url += ("category=" + category.getValue());

            }

            if (!hideSource.getValue()) {
                url += ("sources=" + source.getValue());
            }
        } else {
            if (keyword.getValue() != null && !keyword.getValue().isEmpty()) {

                url += ("q=" + keyword.getValue());

            }
        }
        url += ("&apiKey=" + BuildConfig.API_KEY);

        remoteRepository.getTopHeadLines(url, new ApiCall() {
            @Override
            public void onSuccess(ApiResponsePojo apiResponsePojo) {
                newsResponseMutableLiveData.postValue(apiResponsePojo);
            }

            @Override
            public void onFailure() {
                commonModel.showToastMessage("Something Went Wrong..!");
            }
        });

        return true;
    }

    public void callApi() {
        newsResponseMutableLiveData = remoteRepository.getTopHeadLines();
    }

    public interface ApiCall {
        void onSuccess(ApiResponsePojo apiResponsePojo);

        void onFailure();
    }
}
